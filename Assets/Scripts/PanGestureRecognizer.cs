﻿using System;
using System.Collections.Generic;
using UnityEngine;

#region IPanGestureRecognizer

    public interface IPanGestureRecognizer
    {
        void PanGestureRecognizerDidRotate(PanGestureRecognizer recognizer, float angles);
    }

#endregion

public class PanGestureRecognizer : MonoBehaviour
{
    [Range(1, 10)]
    public float speed = 1;

    [Range(0, 10)]
    public float damping = 1;

    [SerializeField]
    private float limit;

    [HideInInspector]
    public IPanGestureRecognizer panDelegate;
    
    private bool isTouching = false;
    public List<float> positionHistory;
    private float velocity;

    #region Life Cycle

        private void Start()
        {
            positionHistory = new List<float>();
        }

        private void Update()
        {
            if (!isTouching && Input.GetMouseButtonDown(0))
            {
                AddPositionToHistory(Input.mousePosition.x);
                isTouching = true;
            }
            else if (isTouching && Input.GetMouseButtonUp(0))
            {
                float delta = (Input.mousePosition.x - positionHistory[positionHistory.Count - 1]) / positionHistory.Count;
                float vel = delta * Time.deltaTime * (speed * 0.5f);
                float sign = vel < 0 ? -1 : 1;
                vel = Math.Min(Math.Abs(vel), limit);
                velocity = vel * sign;
                isTouching = false;
            }

            if (isTouching)
            {
                float lastPosition = GetLastPosition();
                velocity = 0;
                float delta = Input.mousePosition.x - lastPosition;
                RotateBy(UnitsToAngles(delta));
                AddPositionToHistory(Input.mousePosition.x);
            }
            else if (velocity != 0)
            {
                float delta = velocity / Time.deltaTime;
                RotateBy(UnitsToAngles(delta));
                float ratio = (10 - damping) * 0.1f;
                float damped = velocity * ratio;
                if (Math.Abs(damped) < 0.01f)
                {
                    damped = 0;
                }

                velocity = damped;
            }
        }

    #endregion



    #region Helper

        private void AddPositionToHistory(float position)
        {
            positionHistory.Insert(0, position);
            if (positionHistory.Count > 3)
            {
                positionHistory.RemoveRange(3, positionHistory.Count - 3);
            }
        }

        private float GetLastPosition()
        {
            if (positionHistory.Count > 0)
            {
                return positionHistory[0];
            }

            return 0;
        }

        private float UnitsToAngles(float unit)
        {
            return unit * (speed * 0.1f);
        }
        
        private void RotateBy(float angles)
        {
            if (panDelegate != null)
            {
                panDelegate.PanGestureRecognizerDidRotate(this, angles);
            }
        }
        
    #endregion
    
}
